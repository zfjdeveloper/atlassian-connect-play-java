package com.atlassian.connect.play.java.util;

import com.fasterxml.jackson.databind.JsonNode;
import play.Logger;

public final class Utils
{
    public static final Logger.ALogger LOGGER = Logger.of("ac");

    private Utils()
    {
    }

    /**
     * Get AccountId from JsonNode
     * @param jsonNode
     * @return
     */
    public static String getAccountId(JsonNode jsonNode){
        if(jsonNode.has("context") && jsonNode.get("context").has("user")
                && jsonNode.get("context").get("user").has("accountId")){
            String accountId = new String(jsonNode
                    .get("context")
                    .get("user")
                    .get("accountId").asText());
            return accountId;
        }else if(jsonNode.has("sub")){
            return jsonNode.get("sub").asText();
        }
        return null;
    }
}
